var main = {

    init: function () {
        main.scroll();
        main.scrollToTop();
        main.animateOnScroll();
        main.aosInit();
        main.counterUp();
        main.windowSize();
        main.customCarousel1();
        main.stickyHeader();
    },


    // Sticky header
    stickyHeader: function() {
        ($header = $("#the_header")),
        $(window).scroll(function () {
            50 < $(this).scrollTop() ? $header.addClass("sticked") : $header.removeClass("sticked");
        });
    },


    scroll: function () {
        $('.scrollTo').on('click', function(event) {
            var target = $(this.getAttribute('href'));
            if( target.length ) {
                // event.preventDefault();
                $('html, body').stop().animate({
                    scrollTop: target.offset().top
                },{ 
                    duration:1,
                    easing:"linear",
                });
            }
        });
    },

    scrollToTop: function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#up').fadeIn();
            } else {
                $('#up').fadeOut();
            }
        });
    },
    
    // INITIALIZE AOS
    aosInit: function () {
        AOS.init({
            once: true
        });
    },

    // Numbers counter up
    counterUp: function () {
        var timerRan = false;
        $(window).on("scroll", function(){
        if( ($(window).scrollTop() + $(window).height() > $(".modulotech-numbers").position().top) && !timerRan ){
            $('.timer').countTo();
            timerRan = true;
        }
        });
    },


    // Animate OnScroll 
    animateOnScroll: function() {
        $(window).scroll(function() {
            $(".revealOnScroll:not(.animated)").each(function(){
                var objPos = $(this).offset().top;
                var window_height = $(window).height();
                var topOfWindow = $(window).scrollTop();
                if (objPos < topOfWindow+window_height) {
                    $(this).addClass('animated ' + $(this).data('animation'));
                }
            });
        });
    },

    // onglets slider
    customCarousel1: function() {
        $(document).ready(function(){
            // main slider set up
            $('.sync-img-slider').slick({
                dots: false,
                centerMode: true,
                centerPadding: '0px',
                arrows:false,
                slidesToShow: 1,
                draggable:false,
                fade:true,

            });
             // small nav sync
            $(document).on("click",".nav-slider .smallnav-item",function(){
                var di = $(this).data("index");
               $( '.sync-img-slider' ).slick('slickGoTo', di);
   
               $('.smallnav-item').attr('class', 'smallnav-item');
               $('.smallnav-item[data-index="'+ di +'"]').attr('class', 'smallnav-item active');
           });
           
        });

    },


     // Window size
    windowSize: function (){
        var width = $(window).width();
        $(window).resize(function() {
            width = $(window).width();
        });
        return width;
    },

}

$(document).ready(function () {
    main.init();
});